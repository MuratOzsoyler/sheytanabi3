module Main where

import Prelude

import Control.Monad.RWS (tell)
import Control.Monad.Writer (Writer, runWriter, pass)
import Data.Array ((..))
import Data.Array as Array
import Data.Foldable (for_)
import Data.HashSet (HashSet)
import Data.HashSet as HashSet
import Data.List (List(..), (:))
import Data.List as List
import Data.Tuple (Tuple(..))
import Debug as Debug
import Effect (Effect)
import Effect.Class.Console as Console

type Value = Int
type CellNum = Int

maxValue :: Value
maxValue = 10 -- 30

maxCells :: CellNum
maxCells = 5

type Column = HashSet Value
type Columns = List Column

type Result =
  { columns :: Columns
  , usedPairs :: UsedPairs
  }

type Results = List Result

type UsedPairs = HashSet (Tuple Value Value)
type UnusedPairs = UsedPairs
type UsedValues = HashSet Value

valueUniverse :: HashSet Int
valueUniverse = HashSet.fromArray (1 .. maxValue)

usedPairUniverse :: HashSet (Tuple Value Value)
usedPairUniverse =
  let
    pairs = do
      parent <- 1 .. maxValue
      child <- 1 .. maxValue
      pure $ Tuple parent child
  in
    HashSet.fromArray pairs

wander :: Unit -> Results
wander _ =
  -- unsafeCrashWith "not implemented"
  go 2 Nil
  where
  -- usageFlags = initUsageFlags maxValue

  go :: Value -> Results -> Results
  go child results
    | child > maxValue = results
    | otherwise =
        let
          Tuple _ result = Debug.trace "go ->  goDeep 1 child HashSet.empty HashSet.empty" \_ -> runWriter $ goDeep 1 child HashSet.empty HashSet.empty -- unsafeCrashWith "go ohhoooo"
          results' = result : results
        in
          {- go (child + 1) -} results'

  goDeep :: Value -> Value -> UsedPairs -> Column -> Writer Result Unit
  goDeep parent child used column
    | parent > maxValue = pure unit
    | child > maxValue || parent `HashSet.member` column = Debug.trace "goDeep -> goDeep (parent + 1) (parent + 2) used column" \_ -> goDeep (parent + 1) (parent + 2) used column
    | otherwise = Debug.trace "goDeep -> goShallow parent child used column" \_ -> goShallow parent child used column

  goShallow :: Value -> Value -> UsedPairs -> Column -> Writer Result Unit
  goShallow parent child used column
    | child > maxValue = Debug.trace "goShallow (1) -> goDeep (parent + 1) (parent + 2) used column" \_ -> goDeep (parent + 1) (parent + 2) used column
    | child `HashSet.member` column || Tuple parent child `HashSet.member` used = Debug.trace "goShallow (2) -> goShallow parent (child + 1) used column" \_ -> goShallow parent (child + 1) used column
    | otherwise = case HashSet.size column of
        c
          | c == maxCells -> pure unit
          | diff <- maxCells - c, diff <= 2 -> do
              let used' = Tuple parent child `HashSet.insert` used
              let column' = parent `HashSet.insert` column
              if HashSet.size column' == maxCells then do
                -- tell $ List.singleton { column: column', usedPairs: used' }
                pass $ pure $ Tuple unit \{ columns } -> { columns: (column' : columns), usedPairs: used' }
                let column'' = child `HashSet.insert` column
                -- tell $ List.singleton { column: column'', usedPairs: used' }
                pass $ pure $ Tuple unit \{ columns } -> { columns: (column'' : columns), usedPairs: used' }
              else do
                let column'' = child `HashSet.insert` column'
                -- tell $ List.singleton { column: column'', usedPairs: used' }
                pass $ pure $ Tuple unit \{ columns } -> { columns: (column'' : columns), usedPairs: used' }
              Debug.trace "*** goShallow -> goShallow parent (child + 1) used column" \_ -> goShallow parent (child + 1) used column
          | otherwise -> do
              let used' = Tuple parent child `HashSet.insert` used
              let column' = parent `HashSet.insert` (child `HashSet.insert` column)
              Debug.trace "goShallow (3) -> goDeep (parent + 1) (parent + 2) used' column'" \_ -> goDeep (parent + 1) (parent + 2) used' column'
              Debug.trace "goShallow (4) -> goShallow parent (child+ 1) used column" \_ -> goShallow parent (child + 1) used column

main :: Effect Unit
main = do
  Console.log "🍝"
  wander unit `for_` \{ columns, usedPairs } -> do
    let
      -- col = HashSet.toArray column # Array.sort
      (col :: Array (Array Value)) = columns # map HashSet.toArray # List.toUnfoldable
      up = HashSet.toArray usedPairs # Array.sort <#> \(Tuple p c) -> [ p, c ]
    Console.log $ "column=" <> show col <> ", usedPairs=" <> show up
